package br.com.syonet.exercicio01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		List<Integer>listaNumeros = new ArrayList<>();
		listaNumeros.addAll(Arrays.asList(4,7,13,34,1,23));
		
		listaNumeros = listarNumerosOrdenados(listaNumeros);
		
		listaNumeros.forEach(System.out::println);

	}
	
	static List<Integer> listarNumerosOrdenados(List<Integer> numeros){
		List<Integer> numeroOrdenados = numeros.stream().sorted().collect(Collectors.toList());
		return numeroOrdenados;
	}

}
