package br.com.syonet.exercicio02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {

		List<String> listaDeNomes = new ArrayList<String>();

		listaDeNomes.addAll(Arrays.asList("Hélio", "Louise", "Isabel", "Carlos", "Luis", "Leo"));

		listaDeNomes.forEach(System.out::println);

		System.out.println("-------------------------\n");

		listaDeNomes = nomesPorCaracter(listaDeNomes, 'L');
		listaDeNomes.forEach(System.out::println);
	}

	static List<String> nomesPorCaracter(List<String>nomes, char caracterInicial){
		List<String> nomesInciadosComCaracter = nomes
						.stream()
						.filter(nome -> nome.charAt(0)==caracterInicial)
						.sorted()
						.collect(Collectors.toList());
		return nomesInciadosComCaracter;
	}

}
