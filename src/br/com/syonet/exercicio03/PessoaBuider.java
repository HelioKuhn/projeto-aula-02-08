package br.com.syonet.exercicio03;

public class PessoaBuider {

	private String nome;
	private String dataNascimento;
	private String cidade;

	public PessoaBuider nome(String nome) {
		this.nome = nome;
		return this;
	}

	public PessoaBuider dataNascinto(String dataNascimento) {
		this.dataNascimento = dataNascimento;
		return this;
	}

	public PessoaBuider cidade(String cidade) {
		this.cidade = cidade;
		return this;
	}

	public Pessoa buid() {
		return new Pessoa(nome, dataNascimento, cidade);
	}

}
