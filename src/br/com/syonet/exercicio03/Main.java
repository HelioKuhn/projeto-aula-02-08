package br.com.syonet.exercicio03;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		Pessoa pessoa1 = new PessoaBuider()
				.nome("Hélio")
				.dataNascinto("16/01/1991")
				.cidade("Montenegro")
				.buid();
		
		
		Pessoa pessoa2 = new PessoaBuider()
				.nome("Lucas")
				.dataNascinto("14/03/2014")
				.cidade("Tramandaí")
				.buid();
		
		Pessoa pessoa3 = new PessoaBuider()
				.nome("João")
				.dataNascinto("09/05/1980")
				.cidade("São Paulo")
				.buid();
		
		Pessoa pessoa4 = new PessoaBuider()
				.nome("Rafaela")
				.dataNascinto("15/07/2009")
				.cidade("Tramandaí")
				.buid();
		
		
		List<Pessoa> listaPessoas =  Arrays.asList(pessoa1,pessoa2,pessoa3,pessoa4);
		
		listaPessoas.forEach(System.out::println);
		
		
		listaPessoas =  Pessoa.pessoasMaiorDeIdade(listaPessoas);
		
		System.out.println("------------Lista só como Pessoas maiores de Idade-------------\n");
		
		listaPessoas.forEach(System.out::println);
	}

}
