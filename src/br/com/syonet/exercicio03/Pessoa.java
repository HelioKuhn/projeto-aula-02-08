package br.com.syonet.exercicio03;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class Pessoa {

	private String nome;
	private LocalDate dataNascimento;
	private String cidade;
	private Integer idade;
	private DateTimeFormatter sdf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	public Pessoa(String nome, String dataNascimento, String cidade) {
		super();
		this.nome = nome;
		this.dataNascimento = LocalDate.parse(dataNascimento, sdf);
		calculaIdade(LocalDate.parse(dataNascimento, sdf));
		this.cidade = cidade;
	}

	public Integer getIdade() {
		return idade;
	}
	
	public String getDataNascimento() {
		return  dataNascimento.format(sdf);
	}

	
	public String toString() {
		return "Nome: " + nome + "\n" + "Data Nascimento: " + getDataNascimento() + "\n" + "Cidade: " + cidade 
				+ "\n"
				+ "Idade: " + idade + "\n";
				

	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = LocalDate.parse(dataNascimento, sdf);
		calculaIdade(this.dataNascimento);
	}

	public void calculaIdade(LocalDate dataNascimento) {
		LocalDate dataAtual = LocalDate.now();

		if (dataAtual.getMonth().getValue() > dataNascimento.getMonthValue()) {
			this.idade = dataAtual.getYear() - dataNascimento.getYear();
			return;
		}
		if (dataAtual.getMonth().getValue() == dataNascimento.getMonthValue()
				&& dataAtual.getDayOfMonth() >= dataNascimento.getDayOfMonth()) {
			this.idade = dataAtual.getYear() - dataNascimento.getYear();
		} else {
			this.idade = dataAtual.getYear() - dataNascimento.getYear() - 1;
		}
	}
	
	public static List<Pessoa> pessoasMaiorDeIdade(List<Pessoa> lista){
		List<Pessoa> listaPessoasMaiores = lista.stream().filter(p -> p.idade >= 18)
				.collect(Collectors.toList());
		return listaPessoasMaiores;
	}
	

}
