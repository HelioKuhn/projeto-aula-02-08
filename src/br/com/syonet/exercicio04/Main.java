package br.com.syonet.exercicio04;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Main {

	public static void main(String[] args) {
		Veiculo veiculo1 = new VeiculoBuider()
				.marca("HONDA")
				.modelo("CIVIC")
				.chassi("XH899DYGKS")
				.buid();
		
		
		Veiculo veiculo2 = new VeiculoBuider()
				.marca("TOYOTA")
				.modelo("COROLLA")
				.chassi("DSDFXKS83")
				.buid();
		
		
		Veiculo veiculo3 = new VeiculoBuider()
				.marca("ALFA ROMEO")
				.modelo("GIULIA")
				.chassi("HPSKJXKA")
				.buid();
		
		List<Veiculo> veiculos = Arrays.asList(veiculo1,veiculo2, veiculo3);
		
		
		System.out.println("Buscar veículo");
		Optional<Veiculo> veiculoChassi = Veiculo.encontrarVeiculoChassi(veiculos, "DSDFXKS83");
		
		if(veiculoChassi.isPresent()) {
			System.out.printf("Dados do veículo com chassi %s " +veiculoChassi.get(), veiculoChassi.get().getChassi().get() );
		}
		else {
			System.out.println("Veículo Não encontrado!!!");
		}
		
		}

}
	