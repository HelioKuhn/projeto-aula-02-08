package br.com.syonet.exercicio04;

import java.util.List;
import java.util.Optional;

public class Veiculo {

	private String marca;
	private String modelo;
	private String chassi;

	public Veiculo(String marca, String modelo, String chassi) {
		this.marca = marca;
		this.modelo = modelo;
		this.chassi = chassi;
	}

	public String toString() {
		return "\nMarca: " + marca + "\n" + "Modelo: " + modelo + "\n" + "Chassi: " + chassi + "\n";
	}

	public static Optional<Veiculo> encontrarVeiculoChassi(List<Veiculo> veiculos, String chassi) {
		Optional<Veiculo> veiculo =  veiculos.stream().filter(v -> v.getChassi().get().equals(chassi)).findAny();
		if(veiculo.isPresent()) {
			return Optional.ofNullable(veiculo).get();
		}
		else {
			return Optional.empty();
		}
	}

	public Optional<String> getChassi() {
		return Optional.ofNullable(chassi);
	}

}