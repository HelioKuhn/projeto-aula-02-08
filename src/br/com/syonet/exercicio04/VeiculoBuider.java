package br.com.syonet.exercicio04;

public class VeiculoBuider {

	private String marca;
	private String modelo;
	private String chassi;
	
	public VeiculoBuider marca(String marca) {
		this.marca = marca;
		return this;
	}
	
	public VeiculoBuider modelo(String modelo) {
		this.modelo = modelo;
		return this;
	}
	
	public VeiculoBuider chassi(String chassi) {
		this.chassi = chassi;
		return this;
	}
	
	public Veiculo buid() {
		return new Veiculo(marca, modelo, chassi);
	}
}
