package br.com.syonet.exercicio05;

public class PessoaBuider {
	
	private String nome;
	private String sobrenome;
	private String dataNascimento;
	private Cidade cidade;
	
	public PessoaBuider() {
		cidade = new Cidade(null, null);
	}
	
	public PessoaBuider nome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public PessoaBuider sobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
		return this;
	}
	
	public PessoaBuider dataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
		return this;
	}
	
	public PessoaBuider cidade(String cidade) {
		this.cidade.setNomeCidade(cidade);
		return this;
	}
	
	public PessoaBuider estado(Estado estado) {
		this.cidade.setEstado(estado);
		return this;
	}
	
	public Pessoa build() {
		return new Pessoa(nome, sobrenome, dataNascimento, cidade.getNomeCidade(), cidade.getEstado());
	}
	
}
