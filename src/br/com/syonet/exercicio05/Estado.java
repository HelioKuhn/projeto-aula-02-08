package br.com.syonet.exercicio05;

public enum Estado {

	RS,
	SC,
	PR,
	SP,
	RJ,
	BA,
	TO,
	AM,
	AP
}
