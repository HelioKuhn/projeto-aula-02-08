package br.com.syonet.exercicio05;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		Pessoa pessoa1 = new PessoaBuider()
				.nome("Helio")
				.sobrenome("Kuhn")
				.dataNascimento("16/01/1991")
				.cidade("Montenegro")
				.estado(Estado.RS)
				.build();
		
		
		Pessoa pessoa2 = new PessoaBuider()
				.nome("João")
				.sobrenome("Lucas")
				.dataNascimento("23/05/2003")
				.cidade("Florianópolis")
				.estado(Estado.SC)
				.build();
		
		Pessoa pessoa3 = new PessoaBuider()
				.nome("Rafala")
				.sobrenome("Lopes")
				.dataNascimento("08/12/2000")
				.cidade("São Paulo")
				.estado(Estado.SP)
				.build();
		
		Pessoa pessoa4 = new PessoaBuider()
				.nome("Luiza")
				.sobrenome("Santos")
				.dataNascimento("10/07/1998")
				.cidade("Porto Alegre")
				.estado(Estado.RS)
				.build();
		
		Pessoa pessoa5 = new PessoaBuider()
				.nome("Amanda")
				.sobrenome("Ferreira")
				.dataNascimento("14/09/1988")
				.cidade("Guarulhos")
				.estado(Estado.SP)
				.build();
		
		
		List<Pessoa> listaPessoas= Arrays.asList(pessoa1, pessoa2,pessoa3,pessoa4,pessoa5);
		
		listaPessoas.forEach(System.out::println);
		
		System.out.println("-------Buscar por estado-------\n");
		
		List<Pessoa> listaPessoasEstado = Pessoa.buscarPorEstado(listaPessoas, Estado.SP);
		
		listaPessoasEstado.forEach(System.out::println);
		

	}

}
