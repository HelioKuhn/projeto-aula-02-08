package br.com.syonet.exercicio05;

import java.util.List;
import java.util.stream.Collectors;

public class Pessoa {
	
	private String nome;
	private String sobrenome;
	private String dataNascimento;
	private Cidade cidade;

	
	public Pessoa(String nome, String sobrenome, String dataNascimento, String cidade, Estado estado) {
		super();
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.dataNascimento = dataNascimento;
		this.cidade = new Cidade(cidade, estado);
	}
	
	public String toString() {
		return "Nome Completo: " + nome + " " + sobrenome +"\n"
				+ "Data Nascimento: " + dataNascimento + "\n"
				+ cidade.toString() + "\n";
	}
	
	public static List<Pessoa> buscarPorEstado(List<Pessoa>pessoas, Estado estado){
		List<Pessoa> pessoasEstado = pessoas.stream()
				.filter(p -> p.cidade.getEstado()==estado).collect(Collectors.toList());
		return pessoasEstado;
	}

}
